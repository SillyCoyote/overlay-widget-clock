// yy.mm.dd-hhmmss


const timeElement = document.querySelector(".time");
const dateElement = document.querySelector(".date");

/**
 * @param {Date} date
 */
function formatTime(date) {
    const Hour = date.getHours().toString().padStart(2, "0");
    //const hours12 = Hour % 12 || 12;
    const minutes = date.getMinutes().toString();
    //const isAm = date.getHours() < 12;
    const Seconds = date.getSeconds().toString().padStart(2, "0");

    return `${Hour}:${minutes}:${Seconds}`;
}

/**
 * @param {Date} date
 */
function formatDate(date) {
    const Year = date.getFullYear().toString().substr(-2);
    const Month = (date.getMonth() + 1 ).toString().padStart(2, "0");
    //const Month = date.getMonth().toString().substr(-2);
    //const Day = ("0" + (date.getDate())).slice(-2);
    const Day = date.getDate().toString().padStart(2, "0");

    const DAYS = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ];
    const MONTHS = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ];

    return `${Year}.${Month}.${Day}`;
}

setInterval(() => {
const now = new Date();
//console.log(now);
timeElement.textContent = formatTime(now);
dateElement.textContent = formatDate(now);
}, 200);
